# -*- coding: utf-8 -*-

import config
import telebot
import datetime
import sqlite3
import logging

###
bot = telebot.TeleBot(config.token)
me = "86163709"
club = "Real Madrid"
###

logger = telebot.logger
telebot.logger.setLevel(logging.INFO)

################################################################
'''######################START###############################'''
################################################################
@bot.message_handler(commands=["start"])
def handle_start(message):
    id = message.chat.id
    conn = sqlite3.connect('userss.db')
    c = conn.cursor()

    c.execute("SELECT chatidd FROM userss WHERE chatidd=?", (id, ))
    dEx = c.fetchone()

    if dEx == None: #проверка
        c.execute("INSERT INTO userss (chatidd, utcc, namee, nicknamee) VALUES (?, ?, ?, ?)", (id, 0, message.chat.first_name, message.chat.username))
        conn.commit()
        print(message.chat.first_name + " (" + str(message.chat.id) + ") " + "new user" + " at " + str(datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M UTC")))
        bot.send_message(me, "new user")
    else:
        bot.send_message(message.chat.id, "Welcome back to <i>Madridd!</i>" + "\n" + "/help", parse_mode="HTML")

################################################################
'''######################HELP################################'''
################################################################
@bot.message_handler(commands=["help"])
def handle_help(message):
    bot.send_message(message.chat.id, "Use the following commands" + "\n" * 2
                     + "/next - <i>next match</i>" + "\n"
                     + "/timeset - <i>set the time</i>",
                     parse_mode="HTML"
                     )

################################################################
'''######################NEXT################################'''
################################################################
@bot.message_handler(commands=["next"])
def handle_text(message):
    id = message.chat.id
    user_murkup = telebot.types.ReplyKeyboardMarkup(True, False)

    conn2 = sqlite3.connect('userss.db')
    c2 = conn2.cursor()
    c2.execute("UPDATE userss SET namee=?, nicknamee=? WHERE chatidd=?", (message.chat.first_name, message.chat.username, id))
    conn2.commit()
    c2.execute("SELECT utcc FROM userss WHERE chatidd=?", (id, ))
    dDelta = c2.fetchone()
    if dDelta == None:
        c2.execute("INSERT INTO userss (chatidd, utcc) VALUES (?, ?)", (id, 0))
        bot.send_message(me, "Error 57")


    dDelta = dDelta[0]
    dDelta = datetime.timedelta(hours=dDelta)

    conn = sqlite3.connect('primeraa.db')
    c = conn.cursor()

    c.execute("SELECT competitionn FROM primeraa WHERE datetimee>=? ORDER BY datetimee ASC", (datetime.datetime.utcnow(), ))
    dCompetition = c.fetchone()
    if dCompetition == None:
        bot.send_message(id, "Sorry, error 75")
    else:
        rCompetition = str(dCompetition[0])

    c.execute("SELECT roundd FROM primeraa WHERE datetimee>=? ORDER BY datetimee ASC", (datetime.datetime.utcnow(), ))
    dRound = c.fetchone()
    if dRound == None:
        bot.send_message(id, "Sorry, error 82")
    else:
        rRound = str(dRound[0])

    rFirst = config.sLaliga + rCompetition + ", " + rRound + "\n"

    c.execute("SELECT homee FROM primeraa  WHERE datetimee>=? ORDER BY datetimee ASC", (datetime.datetime.utcnow(),))
    dHome = c.fetchone()
    c.execute("SELECT teamm FROM primeraa WHERE datetimee>=? ORDER BY datetimee ASC", (datetime.datetime.utcnow(), ))
    dTeam = c.fetchone()
    #dHome = rHome = config.sInfo + "Real Madrid - " + str(dTeam[0]) + "\n"
    if str(dHome[0]) == 'true':
        rHome = config.sInfo + club + " - " + str(dTeam[0]) + "\n"
    elif str(dHome[0]) == 'false':
        rHome = config.sInfo + str(dTeam[0]) + " - " + club + "\n"

    c.execute("SELECT stadiumm FROM primeraa WHERE datetimee>=? ORDER BY datetimee ASC", (datetime.datetime.utcnow(), ))
    dStadium = c.fetchone()
    c.execute("SELECT cityy FROM primeraa WHERE datetimee>=? ORDER BY datetimee ASC", (datetime.datetime.utcnow(), ))
    dCity = c.fetchone()
    rLocation = config.sLocation + str(dStadium[0]) + ", " + str(dCity[0]) + "\n"

    c.execute("SELECT datetimee FROM primeraa WHERE datetimee>=? ORDER BY datetimee ASC", (datetime.datetime.utcnow(), ))
    dDatetime = c.fetchone()
    dDatetime = dDatetime[0]

    dDatetime = datetime.datetime.strptime(dDatetime, "%Y-%m-%d %H:%M") + dDelta

    rDate = config.sCalendar + dDatetime.strftime("%B %d, %Y (%a)") + "\n"
    rTime = config.sClock + dDatetime.strftime("%H:%M")

    user_murkup = telebot.types.ReplyKeyboardMarkup(True, True)
    user_murkup.row("/next")
    #bot.send_message(86163709, message.chat.first_name + " (" + str(message.chat.id) + ") " + "sent /next" )
    print(message.chat.first_name + " (" + str(message.chat.id) + ") " + "sent /next" + " at " + str(datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M UTC")))
    bot.send_message(message.chat.id,
                    rFirst
                    + rHome
                    + rLocation
                    + rDate
                    + rTime, parse_mode="HTML", reply_markup=user_murkup)

################################################################
'''######################TIMESET#############################'''
################################################################
@bot.message_handler(commands=["timeset"])
def handle_timeset(message):
    id = message.chat.id
    user_murkup = telebot.types.ReplyKeyboardMarkup(True, True)
    user_murkup.row("UTC")
    user_murkup.row("-1", "+1")
    user_murkup.row("-2", "+2")
    user_murkup.row("-3", "+3")
    user_murkup.row("-4", "+4")
    user_murkup.row("-5", "+5")
    user_murkup.row("-6", "+6")
    user_murkup.row("-7", "+7")
    user_murkup.row("-8", "+8")
    user_murkup.row("-9", "+9")
    user_murkup.row("-10", "+10")
    user_murkup.row("-11", "+11")
    user_murkup.row("-12", "+12")
    bot.send_message(id, "Please, choose your timezone" + "\n" +
                     "Now it's <b>{0} UTC </b>".format(datetime.datetime.utcnow().strftime("%d/%m/%Y | %H:%M")),
                     reply_markup=user_murkup, parse_mode="HTML")



@bot.message_handler(content_types=["text"])
def handle_tableset(message):
    if message.text == "UTC":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (0, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "+1":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (1, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "-1":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (-1, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "+2":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (2, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "-2":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (-2, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "+3":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (3, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "-3":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (-3, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "+4":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (4, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "-4":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (-4, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "+5":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (5, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "-5":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (-5, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "+6":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (6, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "-6":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (-6, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "+7":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (7, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "-7":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (-7, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "+8":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (8, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "-8":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (-8, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "+9":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (9, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "-9":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (-9, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "+10":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (10, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "-10":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (-10, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "+11":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (11, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "-11":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (-11, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "+12":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (12, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
    elif message.text == "-12":
        id = message.chat.id
        conn2 = sqlite3.connect('userss.db')
        c2 = conn2.cursor()
        c2.execute("UPDATE userss SET utcc=? WHERE chatidd=?", (-12, id))
        conn2.commit()
        bot.send_message(id, "Timezone is set!" + '\n' + '/next')
