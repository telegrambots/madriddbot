# -*- coding: utf-8 -*-

import telebot
import ssl
import bot
import config

from aiohttp import web

app = web.Application()

# Входная точка всех запросов на указанный порт
# Тут можно добавить какие-то проверки, но для простого случая это не требуется
async def handle(request):
    request_body_dict = await request.json()
    update = telebot.types.Update.de_json(request_body_dict)
    bot.bot.process_new_updates([update])
    return web.Response()

app.router.add_post('/hook', handle)

# Указываем серверу путь к нашему SSL-сертификату и ключу
context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
context.load_cert_chain(config.webhook['ssl_cert'], config.webhook['ssl_key'])

# Запускаем сервер
web.run_app(
    app,
    host=config.webhook['host'],
    port=config.webhook['port'],
    ssl_context=context
)
